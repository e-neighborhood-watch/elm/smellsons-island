# Elm Game Template

We have at this point done a number of game jams in Elm, and we've noticed that the first few steps of developing them are virtually the same.
To avoid having to waste time and brainpower reinventing the wheel every single game jam, we are creating this template to serve as a starting point.
While we do plan to mostly use this for game jams, we expect that we will also use it to aid in regular game development.

## Plans

* Audio
* Basic controls
* Simple placeholder graphics
* Animations

