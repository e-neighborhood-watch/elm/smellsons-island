module Data.Defs
  exposing
    ( defs
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


defs : Svg a
defs =
  Svg.defs
    []
    [ Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/SettingsWheel.svg#content"
      , SvgAttr.id "SettingsWheel"
      , SvgAttr.width "0.6"
      , SvgAttr.height "0.6"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/CancelIcon.svg#content"
      , SvgAttr.id "CancelIcon"
      , SvgAttr.width "0.6"
      , SvgAttr.height "0.6"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/AcceptIcon.svg#content"
      , SvgAttr.id "AcceptIcon"
      , SvgAttr.width "0.6"
      , SvgAttr.height "0.6"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Character.svg#content"
      , SvgAttr.id "Character"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/CharacterEyes.svg#content"
      , SvgAttr.id "CharacterEyes"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Wall.svg#content"
      , SvgAttr.id "Wall"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Block.svg#content"
      , SvgAttr.id "Block"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/LevelIcon.svg#content"
      , SvgAttr.id "LevelIcon"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Lock.svg#content"
      , SvgAttr.id "Lock"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Crown.svg#content"
      , SvgAttr.id "Crown"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Slime/Straight.svg#content"
      , SvgAttr.id "HorizontalSlime"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Slime/Straight.svg#content"
      , SvgAttr.id "VerticalSlime"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      , SvgAttr.transform "rotate(90,0.5,0.5)"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Slime/Corner.svg#content"
      , SvgAttr.id "LeftUpSlime"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Slime/Corner.svg#content"
      , SvgAttr.id "RightUpSlime"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      , SvgAttr.transform "rotate(90,0.5,0.5)"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Slime/Corner.svg#content"
      , SvgAttr.id "RightDownSlime"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      , SvgAttr.transform "rotate(180,0.5,0.5)"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Slime/Corner.svg#content"
      , SvgAttr.id "LeftDownSlime"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      , SvgAttr.transform "rotate(-90,0.5,0.5)"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Slime/End.svg#content"
      , SvgAttr.id "LeftEndSlime"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Slime/End.svg#content"
      , SvgAttr.id "UpEndSlime"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      , SvgAttr.transform "rotate(90,0.5,0.5)"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Slime/End.svg#content"
      , SvgAttr.id "RightEndSlime"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      , SvgAttr.transform "rotate(180,0.5,0.5)"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Slime/End.svg#content"
      , SvgAttr.id "DownEndSlime"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      , SvgAttr.transform "rotate(-90,0.5,0.5)"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Entrance.svg#content"
      , SvgAttr.id "Entrance"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/Exit.svg#content"
      , SvgAttr.id "Exit"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "Data/Assets/MultiTrail.svg#content"
      , SvgAttr.id "MultiTrail"
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
      [
      ]
    ]
