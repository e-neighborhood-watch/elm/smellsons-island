module Data.Levels.Level8
  exposing
    ( level
    )


import Dict
  exposing
    ( Dict
    )


import Data.Color.Type
  as Color
import Data.Text.Type
  as Text
import Level.Half.Type
  exposing
    ( LevelHalf
    )
import Level.Wall.Type
  as Wall


level : LevelHalf
level =
  { wallMap =
    Dict.fromList
      [ ( (1, 2)
        , Wall.MultiTrail []
        )
      , ( (2, 1)
        , Wall.Wall
        )
      , ( (2, 3)
        , Wall.Wall
        )
      ]
  , floorMap =
    Dict.empty
  , bound =
    { x =
      3
    , y =
      5
    }
  , doorLocation =
    { x =
      2
    , y =
      4
    }
  , color =
    Color.Yellow
  }
