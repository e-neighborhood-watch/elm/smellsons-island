module Data.Levels.Level6
  exposing
    ( level
    )


import Dict
  exposing
    ( Dict
    )


import Data.Color.Type
  as Color
import Data.Text.Type
  as Text
import Level.Half.Type
  exposing
    ( LevelHalf
    )
import Level.Wall.Type
  as Wall


level : LevelHalf
level =
  { wallMap =
    Dict.fromList
      [ ( (1, 3)
        , Wall.Block
        )
      ]
  , floorMap =
    Dict.empty
  , bound =
    { x =
      3
    , y =
      5
    }
  , doorLocation =
    { x =
      2
    , y =
      4
    }
  , color =
    Color.Purple
  }
