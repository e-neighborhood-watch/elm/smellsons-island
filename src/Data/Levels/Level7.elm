module Data.Levels.Level7
  exposing
    ( level
    )


import Dict
  exposing
    ( Dict
    )


import Data.Color.Type
  as Color
import Data.Text.Type
  as Text
import Level.Half.Type
  exposing
    ( LevelHalf
    )
import Level.Wall.Type
  as Wall


level : LevelHalf
level =
  { wallMap =
    Dict.fromList
      [ ( (1, 3)
        , Wall.MultiTrail []
        )
      , ( (1, 1)
        , Wall.Wall
        )
      , ( (0, 3)
        , Wall.MultiTrail []
        )
      ]
  , floorMap =
    Dict.empty
  , bound =
    { x =
      3
    , y =
      5
    }
  , doorLocation =
    { x =
      1
    , y =
      2
    }
  , color =
    Color.Yellow
  }
