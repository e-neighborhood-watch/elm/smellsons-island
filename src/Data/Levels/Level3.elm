module Data.Levels.Level3
  exposing
    ( level
    )


import Dict
  exposing
    ( Dict
    )


import Data.Color.Type
  as Color
import Data.Text.Type
  as Text
import Level.Half.Type
  exposing
    ( LevelHalf
    )
import Level.Wall.Type
  as Wall


level : LevelHalf
level =
  { wallMap =
    Dict.fromList
      [ ( (2, 2)
        , Wall.Block
        )
      , ( (1, 0)
        , Wall.Block
        )
      ]
  , floorMap =
    Dict.empty
  , bound =
    { x =
      4
    , y =
      5
    }
  , doorLocation =
    { x =
      0
    , y =
      2
    }
  , color =
    Color.Green
  }
