module Data.Levels.Level11
  exposing
    ( level
    )


import Dict
  exposing
    ( Dict
    )


import Data.Color.Type
  as Color
import Data.Text.Type
  as Text
import Level.Half.Type
  exposing
    ( LevelHalf
    )
import Level.Wall.Type
  as Wall


level : LevelHalf
level =
  { wallMap =
    Dict.fromList
      [ ( (2, 0)
        , Wall.Wall
        )
      , ( (2, 1)
        , Wall.Block
        )
      , ( (2, 2)
        , Wall.Block
        )
      , ( (2, 3)
        , Wall.Block
        )
      , ( (2, 4)
        , Wall.Wall
        )
      ]
  , floorMap =
    Dict.empty
  , bound =
    { x =
      3
    , y =
      5
    }
  , doorLocation =
    { x =
      0
    , y =
      4
    }
  , color =
    Color.Yellow
  }
