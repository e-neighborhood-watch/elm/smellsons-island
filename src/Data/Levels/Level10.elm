module Data.Levels.Level10
  exposing
    ( level
    )


import Dict
  exposing
    ( Dict
    )


import Data.Color.Type
  as Color
import Data.Text.Type
  as Text
import Level.Half.Type
  exposing
    ( LevelHalf
    )
import Level.Wall.Type
  as Wall


level : LevelHalf
level =
  { wallMap =
    Dict.fromList
      [
      ]
  , floorMap =
    Dict.empty
  , bound =
    { x =
      1
    , y =
      5
    }
  , doorLocation =
    { x =
      0
    , y =
      4
    }
  , color =
    Color.Orange
  }
