module Data.Levels.Level5
  exposing
    ( level
    )


import Dict
  exposing
    ( Dict
    )


import Data.Color.Type
  as Color
import Data.Text.Type
  as Text
import Level.Half.Type
  exposing
    ( LevelHalf
    )
import Level.Wall.Type
  as Wall


level : LevelHalf
level =
  { wallMap =
    Dict.fromList
      [ ( (1, 3)
        , Wall.Block
        )
      , ( (0, 0)
        , Wall.Block
        )
      , ( (0, 1)
        , Wall.Block
        )
      , ( (0, 3)
        , Wall.Block
        )
      ]
  , floorMap =
    Dict.empty
  , bound =
    { x =
      3
    , y =
      5
    }
  , doorLocation =
    { x =
      0
    , y =
      2
    }
  , color =
    Color.Orange
  }
