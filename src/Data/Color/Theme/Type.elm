module Data.Color.Theme.Type
  exposing
    ( ThemeColor (..)
    )


import Data.Color.Type
  exposing
    ( Color
    )


type ThemeColor
  = Background
  | Foreground
  | Eyes
  | Color Color
