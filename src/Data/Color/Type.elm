module Data.Color.Type
  exposing
    ( Color (..)
    )


type Color
  = Red
  | Orange
  | Yellow
  | Green
  | Teal
  | Blue
  | Purple
