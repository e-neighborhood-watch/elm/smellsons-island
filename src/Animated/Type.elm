module Animated.Type
  exposing
    ( Animated
    )


type alias Animated a =
  { currentTime :
    Int
  , startingTime :
    Int
  , endingTime :
    Int
  , kind :
    a
  }
