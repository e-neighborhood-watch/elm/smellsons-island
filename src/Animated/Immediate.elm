module Animated.Immediate
  exposing
    ( immediate
    )


import Animated.Type
  exposing
    ( Animated
    )


immediate : Int -> Int -> a -> Animated a
immediate currentTime duration kind =
  { currentTime =
    currentTime
  , startingTime =
    currentTime
  , endingTime =
    currentTime + duration
  , kind =
    kind
  }
