module Update
  exposing
    ( update
    )


import Level.Status.Type
  as Status
import Level.Model.Init
  as Level
import Level.Update
  as Level
import Level.Update.Result.Type
  as Level
import LevelSelect.Model.Complete
  as LevelSelect
import LevelSelect.Model.Level
  as Level
import LevelSelect.Update
  as LevelSelect
import LevelSelect.Update.Result.Type
  as LevelSelect
import Model.Type
  as Model
  exposing
    ( Model
    )
import Model.Suspended.Type
  as SuspendedModel
import Message.Type as Message
  exposing
    ( Message
    )
import Settings.Update
  as Settings
import Settings.Update.Result.Type
  as Settings


update : Message -> Model -> ( Model, Cmd Message )
update message prevModel =
  case
    ( message, prevModel.model )
  of
    -- Don't open the settings twice
    ( Message.OpenSettings, Model.Settings _ _ ) ->
      ( prevModel
      , Cmd.none
      )

    ( Message.OpenSettings, Model.Level levelModel ) ->
      ( { prevModel
        | model =
          Model.Settings
            (SuspendedModel.Level levelModel)
            { settings =
              prevModel.settings
            }
        }
      , Cmd.none
      )

    ( Message.OpenSettings, Model.LevelSelect ) ->
      ( { prevModel
        | model =
          Model.Settings
            SuspendedModel.LevelSelect
            { settings =
              prevModel.settings
            }
        }
      , Cmd.none
      )

    ( Message.Settings settingsMessage, Model.Settings model settingsModel ) ->
      case
        Settings.update settingsMessage settingsModel
      of
        Settings.NoChange ->
          ( prevModel
          , Cmd.none
          )

        Settings.ChangeModel newSettingsModel ->
          ( { prevModel
            | model =
              Model.Settings
                model
                newSettingsModel
            }
          , Cmd.none
          )

        Settings.Cancel ->
          ( { prevModel
            | model =
              case
                model
              of
                SuspendedModel.LevelSelect ->
                  Model.LevelSelect
                SuspendedModel.Level levelModel ->
                  Model.Level levelModel
            }
          , Cmd.none
          )

        Settings.SaveAndExit ->
          ( { prevModel
            | model =
              case
                model
              of
                SuspendedModel.LevelSelect ->
                  Model.LevelSelect
                SuspendedModel.Level levelModel ->
                  Model.Level levelModel
            , settings =
              settingsModel.settings
            }
          , Cmd.none
          )

    ( Message.LevelSelect levelSelectMessage, Model.LevelSelect ) ->
      case
        LevelSelect.update levelSelectMessage prevModel.levels
      of
        LevelSelect.NoChange ->
          ( prevModel
          , Cmd.none
          )

        LevelSelect.Updated levels ->
          ( { prevModel
            | levels =
              levels
            }
          , Cmd.none
          )

        LevelSelect.Selected level ->
          ( { prevModel
            | model =
              level
                |> Level.init
                |> Model.Level
            }
          , Cmd.none
          )

    ( Message.Level levelMessage, Model.Level levelModel ) ->
      case
        Level.update levelMessage levelModel
      of
        Level.NoChange _ ->
          ( prevModel
          , Cmd.none
          )

        Level.InternalChange newLevelModel cmd ->
          ( { prevModel
            | model =
              Model.Level
                newLevelModel
            }
          , Cmd.map Message.Level cmd
          )

        Level.Exit ->
          ( { prevModel
            | model =
              Model.LevelSelect
            }
          , Cmd.none
          )

        Level.Restart ->
          ( { prevModel
            | model =
              prevModel.levels
                |> Level.fromModel
                |> Level.init
                |> Model.Level
            }
          , Cmd.none
          )

        Level.Win ->
          ( { prevModel
            | model =
              Model.LevelSelect
            , levels =
              LevelSelect.complete prevModel.levels
            }
          , Cmd.none
          )

    ( _, _ ) ->
      ( prevModel
      , Cmd.none
      )

