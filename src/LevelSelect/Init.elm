module LevelSelect.Init
  exposing
    ( init
    )


import Level.Status.Type
  as Status
import Level.Half.Type
  exposing
    ( LevelHalf
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import LevelSelect.Option.Type
  exposing
    ( Option
    )


initLevelHalf : LevelHalf -> Option
initLevelHalf =
  Status.add Status.Incomplete


init : LevelHalf -> LevelHalf -> List LevelHalf -> List LevelHalf -> List LevelHalf -> List LevelHalf -> Model
init leftCurrent rightCurrent leftAvailable rightAvailable leftLocked rightLocked =
  { leftSelect =
    { selected =
      initLevelHalf leftCurrent
    , previous =
      []
    , remaining =
      List.map initLevelHalf leftAvailable
    , locked =
      List.map initLevelHalf leftLocked
    }
  , rightSelect =
    { selected =
      initLevelHalf rightCurrent
    , previous =
      []
    , remaining =
      List.map initLevelHalf rightAvailable
    , locked =
      List.map initLevelHalf rightLocked
    }
  }
