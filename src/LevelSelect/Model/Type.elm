module LevelSelect.Model.Type
  exposing
    ( Model
    , WithLevels
    )


import LevelSelect.Option.Type
  as Option
  exposing
    ( Option
    )
import LevelSelect.Select.Type
  as Select
  exposing
    ( Select
    )

type alias Model =
  { leftSelect :
    Select Option
  , rightSelect :
    Select Option
  }


type alias WithLevels m =
  { m
  | levels :
    Model
  }
