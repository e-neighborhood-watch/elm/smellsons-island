module LevelSelect.Model.Level
  exposing
    ( fromModel
    )


import Level.Half.Merge
  as LevelHalf
import Level.Type
  exposing
    ( Level
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )


fromModel : Model -> Level
fromModel model =
  LevelHalf.merge
    model.leftSelect.selected
    model.rightSelect.selected
