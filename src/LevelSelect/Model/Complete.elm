module LevelSelect.Model.Complete
  exposing
    ( complete
    )


import Level.Status.Type
  as Status
import LevelSelect.Option.Type
import LevelSelect.Select.Action
  as Select
import LevelSelect.Select.Type
  exposing
    ( Select
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )


leftComplete : Model -> Model
leftComplete model =
  case
    model.leftSelect.selected.status
  of
    Status.Complete ->
      model
    Status.Incomplete ->
      { leftSelect =
        Select.complete model.leftSelect
      , rightSelect =
        Select.unlock model.rightSelect
      }


swap : Model -> Model
swap model =
  { leftSelect =
    model.rightSelect
  , rightSelect =
    model.leftSelect
  }


complete : Model -> Model
complete model =
  model
  |> leftComplete
  |> swap
  |> leftComplete
  |> swap
