module LevelSelect.Subscriptions
  exposing
    ( subscriptions
    )


import Browser.Events
  as Browser
import Dict


import Keys
import LevelSelect.Message.Type
  exposing
    ( Message (..)
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import Settings.Type
  exposing
    ( Settings
    )


subscriptions : Settings -> Model -> Sub Message
subscriptions settings {} =
  Sub.batch
    [ Dict.singleton settings.controls.levelSelectLeftPrevious LeftPrevious
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Dict.singleton settings.controls.levelSelectLeftNext LeftNext
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Dict.singleton settings.controls.levelSelectRightPrevious RightPrevious
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Dict.singleton settings.controls.levelSelectRightNext RightNext
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Dict.singleton settings.controls.levelSelectSelect Select
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    ]
