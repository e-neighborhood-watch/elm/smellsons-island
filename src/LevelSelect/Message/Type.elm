module LevelSelect.Message.Type
  exposing
    ( Message (..)
    )


type Message
  = LeftPrevious
  | LeftNext
  | RightPrevious
  | RightNext
  | Select
