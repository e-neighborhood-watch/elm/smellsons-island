module LevelSelect.Option.Type
  exposing
    ( Option
    )


import Level.Status.Type
  exposing
    ( WithStatus
    )
import Level.Half.Type
  exposing
    ( LevelHalf
    )


type alias Option =
  WithStatus LevelHalf


