module LevelSelect.Option.Complete
  exposing
    ( complete
    )


import Level.Status.Type
  as Status
import Level.Type
  as Level
import LevelSelect.Option.Type
  exposing
    ( Option
    )


complete : Option -> Option
complete opt =
  { opt
  | status =
    Status.Complete
  }
