module LevelSelect.View
  exposing
    ( view
    )


import Document.Type
  exposing
    ( Document
    )
import Svg.Styled
  as Svg
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Level.Status.Type
  as Status
import Data.Text.Type
  as Text
import LevelSelect.Message.Type
  exposing
    ( Message (..)
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import LevelSelect.Select.View
  as Select
import Settings.Color.Theme
  as Theme
import Settings.Language
  as Language
import Settings.Type
  exposing
    ( Settings
    )


view : Settings -> Model -> Document Message
view settings model =
  let
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.6
        }
      , max =
        { x =
          7.2
        , y =
          8.2
        }
      }
  in
    { title =
      Language.fromText settings.language Text.LevelSelectTitle
    , viewBox =
      viewBox
    , body =
      [ Svg.path
        [ SvgAttr.d
          ( "M .5 "
          ++ (List.length model.leftSelect.previous |> String.fromInt)
          ++ ".5 h 1.5 V 2.5 h 1"
          )
        , SvgAttr.fill "none"
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.strokeWidth "0.1"
        , SvgAttr.strokeLinejoin "round"
        ]
        [
        ]
      , Svg.path
        [ SvgAttr.d
          ( "M 6.5 "
          ++ (List.length model.rightSelect.previous |> String.fromInt)
          ++ ".5 h -1.5 V 2.5 h -1"
          )
        , SvgAttr.fill "none"
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.strokeWidth "0.1"
        , SvgAttr.strokeLinejoin "round"
        ]
        [
        ]
      , Svg.use
        [ SvgAttr.xlinkHref "#LevelIcon"
        , SvgAttr.x "2.7"
        , SvgAttr.y "2"
        , SvgAttr.fill (Theme.toHex settings.theme (Color.Color model.leftSelect.selected.color))
        , SvgAttr.stroke
          ( Theme.toHex settings.theme Color.Foreground )
        ]
        [
        ]
      , Svg.use
        [ SvgAttr.xlinkHref "#LevelIcon"
        , SvgAttr.x "3.3"
        , SvgAttr.y "2"
        , SvgAttr.fill (Theme.toHex settings.theme (Color.Color model.rightSelect.selected.color))
        , SvgAttr.stroke
          ( Theme.toHex settings.theme Color.Foreground )
        ]
        [
        ]
      ]
      ++ Select.view settings 0 model.leftSelect
      ++ Select.view settings 6 model.rightSelect
    }
