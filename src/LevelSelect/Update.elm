module LevelSelect.Update
  exposing
    ( update
    )


import Level.Type
  as Level
import LevelSelect.Message.Type
  exposing
    ( Message (..)
    )
import LevelSelect.Model.Level
  as Level
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import LevelSelect.Select.Action
  as Select


import LevelSelect.Update.Result.Type
  as Update


update : Message -> Model -> Update.Result
update msg model =
  case
    msg
  of
    Select ->
      Level.fromModel model
      |> Update.Selected

    RightPrevious ->
      { model
      | rightSelect =
        Select.prev model.rightSelect
      }
      |> Update.Updated

    RightNext ->
      { model
      | rightSelect =
        Select.next model.rightSelect
      }
      |> Update.Updated

    LeftPrevious ->
      { model
      | leftSelect =
        Select.prev model.leftSelect
      }
      |> Update.Updated

    LeftNext ->
      { model
      | leftSelect =
        Select.next model.leftSelect
      }
      |> Update.Updated
