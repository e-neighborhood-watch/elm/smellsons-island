module LevelSelect.Select.Type
  exposing
    ( Select
    )


type alias Select a =
  { selected :
    a
  , previous :
    List a
  , remaining :
    List a
  , locked :
    List a
  }
