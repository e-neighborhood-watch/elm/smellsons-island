module LevelSelect.Select.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Level.Status.Type
  as Status
import LevelSelect.Option.Type
  exposing
    ( Option
    )
import LevelSelect.Select.Type
  exposing
    ( Select
    )
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )



addIcon : Svg a -> String -> Settings -> Int -> Int -> Svg a
addIcon svg iconName settings x y = 
  Svg.g
    [
    ]
    [ svg
    , Svg.use
      [ SvgAttr.xlinkHref iconName
      , SvgAttr.x (x |> String.fromInt)
      , SvgAttr.y (y |> String.fromInt)
      , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
      ]
      [
      ]
    ]

viewLevelHalf : Settings -> Option -> Int -> Int -> Svg a
viewLevelHalf settings lvlHalf x y =
  let
    levelIcon =
      Svg.use
        [ SvgAttr.xlinkHref "#LevelIcon"
        , SvgAttr.x (x |> String.fromInt)
        , SvgAttr.y (y |> String.fromInt)
        , SvgAttr.fill (Theme.toHex settings.theme (Color.Color lvlHalf.color))
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
        ]
        [
        ]
  in case
    lvlHalf.status
    of
      Status.Complete ->
        addIcon levelIcon "#Crown" settings x y
      Status.Incomplete ->
        levelIcon
      

view : Settings -> Int -> Select Option -> List (Svg a)
view settings x select =
  let
    levels =
      List.reverse select.previous
      ++ select.selected
      :: select.remaining
      ++ select.locked
    
    lockIndex = List.length levels - List.length select.locked
  in
    List.indexedMap
      ( \ y lvlHalf ->
        let
          levelIcon =
            viewLevelHalf settings lvlHalf x y
          in
            if
              y >= lockIndex
            then
              addIcon levelIcon "#Lock" settings x y
            else
              levelIcon
      )
      levels
