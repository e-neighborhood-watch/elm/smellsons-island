module LevelSelect.Select.Action
  exposing
    ( next
    , prev
    , unlock
    , complete
    )


import LevelSelect.Option.Complete
  as Option
import LevelSelect.Option.Type
  exposing
    ( Option
    )
import LevelSelect.Select.Type
  exposing
    ( Select
    )


next : Select a -> Select a
next select =
  case
    select.remaining
  of
    [] ->
      select
    newSelected :: rest ->
      { selected =
        newSelected
      , previous =
        select.selected :: select.previous
      , remaining =
        rest
      , locked =
        select.locked
      }


prev : Select a -> Select a
prev select =
  case
    select.previous
  of
    [] ->
      select
    newSelected :: rest ->
      { selected =
        newSelected
      , previous =
        rest
      , remaining =
        select.selected :: select.remaining
      , locked =
        select.locked
      }


unlock : Select a -> Select a
unlock select =
  case
    select.locked
  of
    [] ->
      select
    newUnlocked :: rest ->
      { selected =
        select.selected
      , previous =
        select.previous
      , remaining =
        select.remaining ++ [ newUnlocked ]
      , locked =
        rest
      }


complete : Select Option -> Select Option
complete select =
  { select
  | selected =
    Option.complete select.selected
  }
