module Level.Half.Merge
  exposing
    ( merge
    )


import Data.Color.Type
  exposing
    ( Color
    )
import Level.Floor.Type
  exposing
    ( Floor
    )
import Level.Wall.Type
  exposing
    ( Wall
    )
import Level.Type
  exposing
    ( Level
    )
import Util.Map.Foldl
  as Map
import Util.Map.Insert
  as Map
import Util.Map.Type
  exposing
    ( Map
    )
import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Position.Translate
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


merge :
  { a
  | floorMap :
    Map Int Floor
  , wallMap :
    Map Int Wall
  , bound :
    Position Int
  , doorLocation :
    Position Int
  , color :
    Color
  } ->
  { a
  | floorMap :
    Map Int Floor
  , wallMap :
    Map Int Wall
  , bound :
    Position Int
  , doorLocation :
    Position Int
  , color :
    Color
  } ->
  Level
merge leftHalf rightHalf =
  { floorMap =
    mergeMap leftHalf.bound.x leftHalf.floorMap rightHalf.floorMap
  , wallMap =
    mergeMap leftHalf.bound.x leftHalf.wallMap rightHalf.wallMap
  , bound =
    { x =
      leftHalf.bound.x + rightHalf.bound.x
    , y =
      max leftHalf.bound.y rightHalf.bound.y
    }
  , playerLocation =
    leftHalf.doorLocation
  , entranceLocation =
    leftHalf.doorLocation
  , exitLocation =
    { x =
      rightHalf.doorLocation.x + leftHalf.bound.x
    , y =
      rightHalf.doorLocation.y
    }
  , divide =
    leftHalf.bound.x
  , color =
    { left =
      leftHalf.color
    , right =
      rightHalf.color
    }
  , history =
    []
  }


mergeMap : Int -> Map Int a -> Map Int a -> Map Int a
mergeMap horizOffset leftMap rightMap =
  let
    offset : Offset Int
    offset =
      { dx =
        horizOffset
      , dy =
        0
      }
  in
    Map.foldl
      ( Position.translate offset >> Map.insert
      )
      leftMap
      rightMap
