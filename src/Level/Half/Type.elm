module Level.Half.Type
  exposing
    ( LevelHalf
    )


import Data.Color.Type
  exposing
    ( Color
    )
import Level.Floor.Type
  exposing
    ( Floor
    )
import Level.Wall.Type
  exposing
    ( Wall
    )
import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.Type
  exposing
    ( Position
    )


type alias LevelHalf =
  { wallMap :
    Map Int Wall
  , floorMap :
    Map Int Floor
  , bound :
    Position Int
  , doorLocation :
    Position Int
  , color :
    Color
  }
