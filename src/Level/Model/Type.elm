module Level.Model.Type
  exposing
    ( Model
    )


import Animated.Type
  exposing
    ( Animated
    )
import Level.Player.Animation.Type
  as Player
import Level.Type
  exposing
    ( Level
    )


type alias Model =
  { level : Level
  , playerAnimation :
    Maybe (Animated Player.Animation)
  }
