module Level.Walls.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )


import Animated.Type
  exposing
    ( Animated
    )
import Level.Player.Animation.Type
  as Player
import Level.Wall.Type
  exposing
    ( Wall
    )
import Level.Wall.View
  as Wall
import Settings.Type
  exposing
    ( Settings
    )
import Util.Map.ToList
  as Map
import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


view : Settings -> Maybe (Position Int, Animated Player.Animation) -> Map Int Wall -> Svg a
view settings pushing floorMap =
  floorMap
  |> Map.toList
  |> List.map
    ( \ (pos, wall) ->
      let
        wallAnimation : Maybe (Animated Player.Animation)
        wallAnimation =
          case
            pushing
          of
            Nothing ->
              Nothing
            Just (animationPosition, animation) ->
              if
                pos == animationPosition
              then
                Just animation
              else
                Nothing
      in
        Wall.view
          settings
          pos
          wallAnimation
          wall
    )
  |> Svg.g []
