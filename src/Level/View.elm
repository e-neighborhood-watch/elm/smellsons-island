module Level.View
  exposing
    ( view
    )


import Css
import Svg.Styled
  as Svg
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Data.Font.Size
  as Font
import Data.Text.Type
  as Text
import Document.Type
  exposing
    ( Document
    )
import Level.Door.Entrance.View
  as Entrance
import Level.Door.Exit.View
  as Exit
import Level.History.Event.Type
  as Event
import Level.IsComplete
  as Level
import Level.Message.Type
  exposing
    ( Message
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Level.Player.Animation.Type
  as PlayerAnimation
import Level.Player.View
  as Player
import Level.Floors.View
  as Floors
import Level.Walls.View
  as Walls
import Settings.Color.Theme
  as Theme
import Settings.Color.Theme.Type
  as Theme
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Language
  as Language
import Settings.Type
  as Settings
  exposing
    ( Settings
    )
import Util.Direction.Invert
  as Direction
import Util.Direction.ToOffset
  as Direction
import Util.Position.Move
  as Position


view : Settings -> Model -> Document Message
view settings { level, playerAnimation } =
  let
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.5 - Font.rawSize Font.Title settings.fontSize
        }
      , max =
        { x =
          toFloat level.bound.x + 1.2
        , y =
          toFloat level.bound.y + 2.5
        }
      }
    rectBox =
      { min =
        { x =
          -0.3
        , y =
          -0.3
        }
      , max =
        { x =
          toFloat level.bound.x + 0.3
        , y =
          toFloat level.bound.y + 0.3
        }
      }
  in
    { title =
      Language.fromText settings.language Text.LevelsTitle
    , viewBox =
      viewBox
    , body =
      [ Svg.text_
        [ Font.toCss Font.Title settings.fontSize
        , SvgAttr.y "-0.45"
        , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
        ]
        [ Svg.text (Language.fromText settings.language Text.LevelsTitle)
        ]
      , Svg.rect
        [ SvgAttr.x (rectBox.min.x |> String.fromFloat)
        , SvgAttr.y (rectBox.min.y |> String.fromFloat)
        , SvgAttr.fill (Theme.toHex settings.theme (Color.Color level.color.left))
        , SvgAttr.width (toFloat level.divide - rectBox.min.x |> String.fromFloat)
        , SvgAttr.height (rectBox.max.y - rectBox.min.y |> String.fromFloat)
        , SvgAttr.stroke "none"
        ]
        [
        ]
      , Svg.rect
        [ SvgAttr.x (level.divide |> String.fromInt)
        , SvgAttr.y (rectBox.min.y |> String.fromFloat)
        , SvgAttr.fill (Theme.toHex settings.theme (Color.Color level.color.right))
        , SvgAttr.width (rectBox.max.x - toFloat level.divide |> String.fromFloat)
        , SvgAttr.height (rectBox.max.y - rectBox.min.y |> String.fromFloat)
        , SvgAttr.stroke "none"
        ]
        [
        ]
      , Svg.rect
        [ SvgAttr.x (rectBox.min.x |> String.fromFloat)
        , SvgAttr.y (rectBox.min.y |> String.fromFloat)
        , SvgAttr.fill "none"
        , SvgAttr.width (rectBox.max.x - rectBox.min.x |> String.fromFloat)
        , SvgAttr.height (rectBox.max.y - rectBox.min.y |> String.fromFloat)
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.strokeWidth "0.1"
        ]
        []
      , Svg.line
        [ SvgAttr.y1 "-0.3"
        , SvgAttr.x1 (level.divide |> String.fromInt)
        , SvgAttr.y2 ((level.bound.y |> String.fromInt) ++ ".3")
        , SvgAttr.x2 (level.divide |> String.fromInt)
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.strokeWidth "0.05"
        , SvgAttr.strokeDasharray "0.1"
        , SvgAttr.strokeDashoffset "0.05"
        ]
        [
        ]
      , Entrance.view settings level.entranceLocation
      , Floors.view settings level.floorMap
      , Exit.view settings level.exitLocation
      , Walls.view
          settings
          ( case
              playerAnimation
            of
              Just { kind } ->
                case
                  kind
                of
                  PlayerAnimation.Pull dir ->
                    Maybe.map2
                      Tuple.pair
                      ( Just
                        ( Position.move (dir |> Direction.invert) level.playerLocation
                        )
                      )
                      playerAnimation

                  PlayerAnimation.Push dir ->
                    Maybe.map2
                      Tuple.pair
                      ( Just
                        ( Position.move dir level.playerLocation
                        )
                      )
                      playerAnimation
                  _ ->
                    Nothing
              _ ->
                Nothing
          )
          level.wallMap
      ]
      ++
        ( if
            Level.isComplete level
            && playerAnimation == Nothing
          then
            []
          else
            [ Player.view
              settings
              ( if
                  level.playerLocation.x < level.divide
                then
                  level.color.left
                else
                  level.color.right
              )
              level.playerLocation
              playerAnimation
            ]
        )
    }
