module Level.Floor.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Level.Floor.Type
  as Floor
  exposing
    ( Floor
    )
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Util.Direction.Type
  as Direction
import Util.Position.Type
  exposing
    ( Position
    )


view : Settings -> Position Int -> Floor -> Svg a
view settings {x, y} floor =
  case
    floor
  of
    Floor.Wall ->
      Svg.use
        [ SvgAttr.xlinkHref "#Wall"
        , SvgAttr.strokeWidth "0"
        , SvgAttr.x (x |> String.fromInt)
        , SvgAttr.y (y |> String.fromInt)
        , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
        ]
        [
        ]
    Floor.Slippery ->
      Svg.use
        [ SvgAttr.xlinkHref "#Slippery"
        , SvgAttr.strokeWidth "0"
        , SvgAttr.x (x |> String.fromInt)
        , SvgAttr.y (y |> String.fromInt)
        , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
        ]
        [
        ]
