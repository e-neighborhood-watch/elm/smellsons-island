module Level.Floor.Type
  exposing
    ( Floor (..)
    )

type Floor
  = Wall
  | Slippery