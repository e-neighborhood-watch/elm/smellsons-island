module Level.Status.Type
  exposing
    ( Status (..)
    , WithStatus
    , add
    )


import Level.Half.Type
  exposing
    ( LevelHalf
    )

type Status
  = Complete
  | Incomplete


type alias WithStatus l =
  { l
  | status :
    Status
  }


add : Status -> LevelHalf -> WithStatus LevelHalf
add status lvl =
  { status =
    status
  , wallMap =
    lvl.wallMap
  , floorMap =
    lvl.floorMap
  , bound =
    lvl.bound
  , doorLocation =
    lvl.doorLocation
  , color =
    lvl.color
  }
