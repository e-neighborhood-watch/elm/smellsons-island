module Level.History.Undo
  exposing
    ( undo
    )


import Level.History.Event.Type
  as Event
import Level.Type
  exposing
    ( Level
    )
import Level.Wall.Trail.Remove
  as Trail
import Level.Wall.Type
  as Wall
import Util.Direction.Invert
  as Direction
import Util.Direction.ToOffset
  as Direction
import Util.Map.Insert
  as Map
import Util.Map.Remove
  as Map
import Util.Position.Move
  as Position
import Util.Position.ToPair
  as Position


undo : Level -> Level
undo level =
  case
    level.history
  of
    [] ->
      level

    Event.Move dir :: oldHistory ->
      { level
      | history =
        oldHistory
      , wallMap =
        Trail.remove level.playerLocation dir level.wallMap
      , playerLocation =
        Position.move (Direction.invert dir) level.playerLocation
      }

    Event.Push dir :: oldHistory ->
      { level
      | history =
        oldHistory
      , wallMap =
        level.wallMap
        |> Trail.remove level.playerLocation dir
        |> Map.insert level.playerLocation Wall.Block
        |> Map.remove (Position.move dir level.playerLocation)
      , playerLocation =
        Position.move (dir |> Direction.invert) level.playerLocation
      }
