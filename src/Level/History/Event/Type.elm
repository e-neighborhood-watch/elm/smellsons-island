module Level.History.Event.Type
  exposing
    ( Event (..)
    )


import Util.Direction.Type
  exposing
    ( Direction
    )

type Event
  = Push Direction
  | Move Direction
