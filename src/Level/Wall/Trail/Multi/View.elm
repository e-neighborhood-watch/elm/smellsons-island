module Level.Wall.Trail.Multi.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Level.Wall.Trail.View
  as Trail
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Util.Direction.Type
  as Direction
  exposing
    ( Direction
    )


view : Settings -> List Direction -> Int -> Int -> List (Svg a)
view settings directions x y =
  let
    wall : Svg a
    wall =
      Svg.use
        [ SvgAttr.xlinkHref "#MultiTrail"
        , SvgAttr.x (x |> String.fromInt)
        , SvgAttr.y (y |> String.fromInt)
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.fill (Theme.toHex settings.theme Color.Background)
        ]
        [
        ]
  in
    case
      directions
    of
      [] ->
        [ wall
        ]
      [ dir1 ] ->
        [ Trail.view settings [dir1] x y
        , wall
        ]
      dir1 :: dir2 :: dirs ->
        Trail.view settings [dir1, dir2] x y
        :: view settings dirs x y
