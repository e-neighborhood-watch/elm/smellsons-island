module Level.Wall.Trail.Remove
  exposing
    ( remove
    )


import Level.Wall.Type
  as Wall
  exposing
    ( Wall
    )
import Util.Direction.Invert
  as Direction
import Util.Direction.ToOffset
  as Direction
import Util.Direction.Type
  exposing
    ( Direction
    )
import Util.Map.Type
  exposing
    ( Map
    )
import Util.Map.Update
  as Map
import Util.Position.Move
  as Position
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


removeNewestTrail : Maybe Wall -> Maybe Wall
removeNewestTrail wall =
  case
    wall
  of
    Nothing ->
      Nothing
    Just (Wall.Trail []) ->
      Nothing
    Just (Wall.Trail [_]) ->
      Nothing
    Just (Wall.Trail (_ :: dirs)) ->
      Just (Wall.Trail dirs)
    Just (Wall.MultiTrail []) ->
      Just (Wall.MultiTrail [])
    Just (Wall.MultiTrail (_ :: dirs)) ->
      Just (Wall.MultiTrail dirs)
    Just _ ->
      wall


remove : Position Int -> Direction -> Map Int Wall -> Map Int Wall
remove currLocation dir wallMap =
  let
    prevLocation =
      Position.move
        ( Direction.invert dir )
        currLocation
  in
    wallMap
    |> Map.update currLocation removeNewestTrail
    |> Map.update prevLocation removeNewestTrail
