module Level.Wall.Trail.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Util.Direction.Type
  as Direction
  exposing
    ( Direction
    )


trailHref : List Direction -> String
trailHref directions =
  case
    directions
  of
    [Direction.North, Direction.South] ->
      "#VerticalSlime"
    [Direction.South, Direction.North] ->
      "#VerticalSlime"

    [Direction.East, Direction.West] ->
      "#HorizontalSlime"
    [Direction.West, Direction.East] ->
      "#HorizontalSlime"

    [Direction.East, Direction.South] ->
      "#RightDownSlime"
    [Direction.South, Direction.East] ->
      "#RightDownSlime"

    [Direction.East, Direction.North] ->
      "#RightUpSlime"
    [Direction.North, Direction.East] ->
      "#RightUpSlime"

    [Direction.West, Direction.South] ->
      "#LeftDownSlime"
    [Direction.South, Direction.West] ->
      "#LeftDownSlime"

    [Direction.West, Direction.North] ->
      "#LeftUpSlime"
    [Direction.North, Direction.West] ->
      "#LeftUpSlime"

    [Direction.North] ->
      "#UpEndSlime"
    [Direction.North, Direction.North] ->
      "#UpEndSlime"

    [Direction.South] ->
      "#DownEndSlime"
    [Direction.South, Direction.South] ->
      "#DownEndSlime"

    [Direction.East] ->
      "#RightEndSlime"
    [Direction.East, Direction.East] ->
      "#RightEndSlime"

    [Direction.West] ->
      "#LeftEndSlime"
    [Direction.West, Direction.West] ->
      "#LeftEndSlime"

    _ ->
      "#LeftUpSlime"


view : Settings -> List Direction -> Int -> Int -> Svg a
view settings directions x y =
  Svg.use
    [ SvgAttr.xlinkHref ( trailHref directions )
    , SvgAttr.strokeWidth "0"
    , SvgAttr.x (x |> String.fromInt)
    , SvgAttr.y (y |> String.fromInt)
    , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
    ]
    [
    ]
