module Level.Wall.Trail.Add
  exposing
    ( add
    )


import Level.Wall.Type
  as Wall
  exposing
    ( Wall
    )
import Util.Direction.Invert
  as Direction
import Util.Direction.ToOffset
  as Direction
import Util.Direction.Type
  exposing
    ( Direction
    )
import Util.Map.Type
  exposing
    ( Map
    )
import Util.Map.Update
  as Map
import Util.Position.Move
  as Position
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


addTrailDirection : Direction -> Maybe Wall -> Maybe Wall
addTrailDirection dir wall =
  case
    wall
  of
    Nothing ->
      Just ( Wall.Trail [dir] )
    Just (Wall.Trail directions) ->
      Just ( Wall.Trail (dir :: directions) )
    Just (Wall.MultiTrail directions) ->
      Just ( Wall.MultiTrail (dir :: directions) )
    otherwise ->
      otherwise


add : Position Int -> Direction -> Map Int Wall -> Map Int Wall
add prevLocation dir wallMap =
  wallMap
  |> Map.update prevLocation (addTrailDirection dir)
  |> Map.update (Position.move dir prevLocation)
    ( dir
    |> Direction.invert
    |> addTrailDirection
    )
