module Level.Wall.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Animated.Type
  exposing
    ( Animated
    )
import Data.Color.Theme.Type
  as Color
import Level.Player.Animation.Type
  as Player
import Level.Player.Animation.Transform
  as Transform
import Level.Wall.Trail.View
  as Trail
import Level.Wall.Trail.Multi.View
  as MultiTrail
import Level.Wall.Type
  as Wall
  exposing
    ( Wall
    )
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Util.Position.Type
  exposing
    ( Position
    )


view : Settings -> Position Int -> Maybe (Animated Player.Animation) -> Wall -> Svg a
view settings {x, y} animation wall =
  case
    wall
  of
    Wall.Block ->
      Svg.use
        [ SvgAttr.xlinkHref "#Block"
        , SvgAttr.strokeWidth "0.5"
        , SvgAttr.x (x |> String.fromInt)
        , SvgAttr.y (y |> String.fromInt)
        , SvgAttr.fill (Theme.toHex settings.theme Color.Background)
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.transform (Transform.fromAnimation animation)
        ]
        [
        ]
    Wall.Trail directions ->
      Trail.view settings directions x y
    Wall.Wall ->
      Svg.use
        [ SvgAttr.xlinkHref "#Wall"
        , SvgAttr.strokeWidth "0"
        , SvgAttr.x (x |> String.fromInt)
        , SvgAttr.y (y |> String.fromInt)
        , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
        ]
        [
        ]
    Wall.MultiTrail directions ->
      Svg.g
        []
        ( MultiTrail.view settings directions x y )
