module Level.Wall.Type
  exposing
    ( Wall (..)
    )


import Util.Direction.Type
  as Direction
  exposing (..)


type Wall
  = Block
  | Wall
  | Trail (List Direction)
  | MultiTrail (List Direction)