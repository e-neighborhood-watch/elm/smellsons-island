module Level.Update
  exposing
    ( update
    )


import Task
import Time


import Animated.Immediate
  as Animated
import Animated.Update
  as Animated
import Util.Direction.ToOffset
  as Direction
import Level.History.Event.Type
  as Event
import Level.History.Undo
  as History
import Level.IsComplete
  as Level
import Level.Message.Type
  as Message
  exposing
    ( Message
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Level.Player.Animation.Type
  as Player
import Level.Player.Update
  as Player
import Level.Player.Update.Result.Type
  as PlayerUpdate
import Level.Update.Result.Type
  as Update
import Util.Box.Within
  as Box
import Util.Direction.Invert
  as Direction
import Util.Offset.Scale
  as Offset
import Util.Offset.ToFloat
  as Offset
import Util.Offset.Type
  exposing
    ( Offset
    )


animationsCommand : Player.Animation -> Cmd Message
animationsCommand playerAnimation =
  Time.now
    |> Task.perform
      ( \ posix ->
        Message.AddAnimations
          { currentTime =
            Time.posixToMillis posix
          , playerAnimation =
            playerAnimation
          }
      )


update : Message -> Model -> Update.Result
update msg { level, playerAnimation } =
  case
    msg
  of
    Message.Noop ->
      Update.NoChange Cmd.none

    Message.Exit ->
      if
        Level.isComplete level
      then
        Update.Win
      else
        Update.Exit

    Message.Restart ->
      Update.Restart

    Message.TimePasses newTime ->
      case
        playerAnimation
      of
        Nothing ->
          Update.NoChange Cmd.none

        Just animation ->
          Update.InternalChange
            { level =
              level
            , playerAnimation =
              Animated.update newTime animation
            }
            Cmd.none

    Message.Undo ->
      case
        level.history
      of
        [] ->
          Update.NoChange Cmd.none
        Event.Move dir :: _ ->
          update
            ( Message.Move 1 (Direction.invert dir))
            { level =
              level
            , playerAnimation =
              playerAnimation
            }
        Event.Push dir :: _ ->
          update
            ( Message.Move 1 (Direction.invert dir))
            { level =
              level
            , playerAnimation =
              playerAnimation
            }


    Message.Move length dir ->
      if
        Level.isComplete level
      then
        Update.Win
      else
        let
          levelBox =
            { min =
              { x =
                0
              , y =
                0
              }
            , max =
              level.bound
            }
        in case
          Player.update
            levelBox
            dir
            { level =
              level
            , playerAnimation =
              playerAnimation
            }
        of
          PlayerUpdate.MoveFailed ->
            Player.FailedMovement dir
              |> animationsCommand
              |> Update.NoChange

          PlayerUpdate.Moved newModel ->
            Player.Movement dir
              |> animationsCommand
              |> Update.InternalChange newModel

          PlayerUpdate.Pushed newModel ->
            Player.Push dir
              |> animationsCommand
              |> Update.InternalChange newModel

          PlayerUpdate.Pulled newModel ->
            Player.Pull dir
              |> animationsCommand
              |> Update.InternalChange newModel


    Message.AddAnimations animation ->
      Update.InternalChange
        { level =
          level
        , playerAnimation =
          Animated.immediate
            animation.currentTime
            75
            animation.playerAnimation
            |> Just
        }
        Cmd.none
