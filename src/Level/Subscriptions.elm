module Level.Subscriptions
  exposing
    ( subscriptions
    )


import Browser.Events
  as Browser
import Dict
import Time


import Keys
import Level.IsComplete
  as Level
import Level.Message.Type
  as Message
  exposing
    ( Message
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Settings.Type
  exposing
    ( Settings
    )
import Util.Direction.Type
  as Direction


subscriptions : Settings -> Model -> Sub Message
subscriptions settings { level, playerAnimation } =
  Sub.batch
    [ case
        playerAnimation
      of
        Nothing ->
          Sub.none

        Just animation ->
          Time.posixToMillis >> Message.TimePasses
            |> Browser.onAnimationFrame
    , if
        Level.isComplete level
      then
        Message.Exit
        |> Dict.singleton settings.controls.levelSelectSelect
        |> Keys.makeKeyDecoder
        |> Browser.onKeyDown
      else
        Sub.none
    , Direction.North
      |> Message.Move 1
      |> Dict.singleton settings.controls.playerMoveNorth
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Direction.South
      |> Message.Move 1
      |> Dict.singleton settings.controls.playerMoveSouth
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Direction.East
      |> Message.Move 1
      |> Dict.singleton settings.controls.playerMoveEast
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Direction.West
      |> Message.Move 1
      |> Dict.singleton settings.controls.playerMoveWest
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Message.Restart
      |> Dict.singleton settings.controls.levelRestart
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Message.Exit
      |> Dict.singleton settings.controls.levelExit
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Message.Undo
      |> Dict.singleton settings.controls.levelUndo
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    ]
