module Level.Type
  exposing
    ( Level
    , get
    )


import Data.Color.Type
  exposing
    ( Color
    )
import Data.Text.Type
  exposing
    ( Text
    )
import Level.Floor.Type
  exposing
    ( Floor
    )
import Level.History.Event.Type
  exposing
    ( Event
    )
import Level.Wall.Type
  exposing
    ( Wall
    )
import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.Type
  exposing
    ( Position
    )


type alias Level =
  { floorMap :
    Map Int Floor
  , wallMap :
    Map Int Wall
  , bound :
    Position Int
  , entranceLocation :
    Position Int
  , exitLocation :
    Position Int
  , playerLocation :
    Position Int
  , divide :
    Int
  , color :
    { left :
      Color
    , right :
      Color
    }
  , history :
    List Event
  }


get :
  { a
  | floorMap :
    Map Int Floor
  , wallMap :
    Map Int Wall
  , bound :
    Position Int
  , entranceLocation :
    Position Int
  , exitLocation :
    Position Int
  , playerLocation :
    Position Int
  , divide :
    Int
  , color :
    { left :
      Color
    , right :
      Color
    }
  , history :
    List Event
  }
    -> Level
get x =
  { floorMap =
    x.floorMap
  , wallMap =
    x.wallMap
  , bound =
    x.bound
  , entranceLocation =
    x.entranceLocation
  , exitLocation =
    x.exitLocation
  , playerLocation =
    x.playerLocation
  , divide =
    x.divide
  , color =
    x.color
  , history =
    x.history
  }
