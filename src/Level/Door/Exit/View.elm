module Level.Door.Exit.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Util.Position.Type
  exposing
    ( Position
    )


view : Settings -> Position Int -> Svg a
view settings {x, y} =
  Svg.use
    [ SvgAttr.xlinkHref "#Exit"
    , SvgAttr.x (x |> String.fromInt)
    , SvgAttr.y (y |> String.fromInt)
    , SvgAttr.fill (Theme.toHex settings.theme Color.Background)
    , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
    , SvgAttr.strokeWidth "0.5"
    ]
    [
    ]
