module Level.Message.Type
  exposing
    ( Message (..)
    )


import Level.Player.Animation.Type
  as Player
import Util.Direction.Type
  exposing
    ( Direction
    )


type Message
  = Noop
  | Exit
  | Undo
  | Restart
  | TimePasses Int
  | Move Int Direction
  | AddAnimations
    { currentTime :
      Int
    , playerAnimation :
      Player.Animation
    }
