module Level.Update.Result.Type
  exposing
    ( Result (..)
    )


import Level.Message.Type
  exposing
    ( Message
    )
import Level.Model.Type
  exposing
    ( Model
    )


type Result
  = NoChange (Cmd Message)
  | InternalChange Model (Cmd Message)
  | Exit
  | Win
  | Restart
