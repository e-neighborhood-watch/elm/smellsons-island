module Level.IsComplete
  exposing
    ( isComplete
    )



import Level.Type
  exposing
    ( Level
    )
import Util.Map.Member
  as Map


isComplete : Level -> Bool
isComplete level =
  if
    level.playerLocation == level.exitLocation
  then
    let
      pairs =
        List.map
          -- Why is List.range inclusive? Who thought this was a good idea?
          ( \x ->
            List.map
              ( \y ->
                { x =
                  x
                , y =
                  y
                }
              )
              ( List.range 0 (level.bound.y - 1) )
          )
          ( List.range 0 (level.bound.x - 1) )
      in
        List.all (\pair -> Map.member pair level.wallMap) (List.concat pairs)
  else
    False
