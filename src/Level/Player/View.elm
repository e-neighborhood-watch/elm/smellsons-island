module Level.Player.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Animated.Type
  exposing
    ( Animated
    )
import Data.Color.Type
  exposing
    ( Color
    )
import Data.Color.Theme.Type
  as Color
import Level.Player.Animation.Type
  as Player
import Level.Player.Animation.Transform
  as Transform
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Util.Position.Type
  exposing
    ( Position
    )


view : Settings -> Color -> Position Int -> Maybe (Animated Player.Animation) -> Svg a
view settings color playerLocation playerAnimation =
  Svg.g
    [ SvgAttr.transform (Transform.fromAnimation playerAnimation)
    ]
    [ Svg.use
      [ SvgAttr.xlinkHref "#Character"
      , playerLocation.x
        |> String.fromInt
        |> SvgAttr.x
      , playerLocation.y
        |> String.fromInt
        |> SvgAttr.y
      , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
      , SvgAttr.stroke (Theme.toHex settings.theme (Color.Color color))
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref "#CharacterEyes"
      , playerLocation.x
        |> String.fromInt
        |> SvgAttr.x
      , playerLocation.y
        |> String.fromInt
        |> SvgAttr.y
      , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
      , SvgAttr.stroke (Theme.toHex settings.theme Color.Eyes)
      ]
      [
      ]
    ]
