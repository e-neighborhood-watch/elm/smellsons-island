module Level.Player.Update
  exposing
    ( update
    )


import Level.Model.Type
  as Level
import Level.Player.Update.Move
  as Player
import Level.Player.Update.Result.Type
  as Update
import Util.Box.Type
  exposing
    ( Box
    )
import Util.Direction.ToOffset
  as Direction
import Util.Direction.Type
  exposing
    ( Direction
    )
import Util.Position.Move
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


update : Box Int -> Direction -> Level.Model -> Update.Result
update levelBox dir ({ level, playerAnimation } as model) =
  let
    newLocation : Position Int
    newLocation =
      Position.move dir level.playerLocation
  in
    Player.move levelBox model dir
