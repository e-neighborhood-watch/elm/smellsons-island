module Level.Player.Update.Move
  exposing
    ( move
    )


import Task
import Time


import Level.History.Event.Type
  as Event
import Level.History.Undo
  as History
import Level.Floor.Type
  exposing
    ( Floor (..)
    )
import Level.Message.Type
  as Message
import Level.Model.Type
  as Level
import Level.Player.Update.Result.Type
  as Update
import Level.Wall.Trail.Add
  as Trail
import Level.Wall.Type
  as Wall
  exposing
    ( Wall
    )
import Util.Box.Type
  exposing
    ( Box
    )
import Util.Box.Within
  as Box
import Util.Direction.ToOffset
  as Direction
import Util.Direction.Type
  exposing
    ( Direction
    )
import Util.Map.Get
  as Map
import Util.Map.Insert
  as Map
import Util.Map.Remove
  as Map
import Util.Map.Type
  exposing
    ( Map
    )
import Util.Position.Move
  as Position
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


backtracking : Position Int -> Direction -> Map Int Wall -> Bool
backtracking playerLocation dir wallMap =
  case
    Map.get playerLocation wallMap
  of
    Just (Wall.Trail (dir1 :: rest)) ->
      dir1 == dir
    Just (Wall.MultiTrail (dir1 :: rest)) ->
      dir1 == dir
    _ ->
      False


move : Box Int -> Level.Model -> Direction -> Update.Result
move levelBox { level, playerAnimation } dir =
  let
    newLocation : Position Int
    newLocation =
      Position.move dir level.playerLocation

  in
    if
      backtracking level.playerLocation dir level.wallMap
    then
      case
        level.history
      of
        Event.Move _ :: _ ->
          Update.Moved
            { level =
              History.undo level
            , playerAnimation =
              playerAnimation
            }

        Event.Push _ :: _ ->
          Update.Pulled
            { level =
              History.undo level
            , playerAnimation =
              playerAnimation
            }

        _ ->
          Update.MoveFailed

    else
      case
        Map.get newLocation level.wallMap
      of
        Nothing ->
          if
            Box.within levelBox newLocation
          then
            Update.Moved
              { level =
                { level
                | playerLocation =
                  newLocation
                , history =
                  Event.Move dir :: level.history
                , wallMap =
                  Trail.add level.playerLocation dir level.wallMap
                }
              , playerAnimation =
                playerAnimation
              }
          else
            Update.MoveFailed
        Just Wall.Block ->
          let
            newBlockLocation : Position Int
            newBlockLocation =
              Position.move dir newLocation
          in
            if
              (Box.within levelBox newLocation)
              && (Box.within levelBox newBlockLocation)
              && Map.get newBlockLocation level.wallMap == Nothing
            then
              Update.Pushed
                { level =
                  { level
                  | playerLocation =
                    newLocation
                  , history =
                    Event.Push dir :: level.history
                  , wallMap =
                    level.wallMap
                    |> Map.insert newBlockLocation Wall.Block
                    |> Map.remove newLocation
                    |> Trail.add level.playerLocation dir
                  }
                , playerAnimation =
                  playerAnimation
                }
            else
              Update.MoveFailed
        Just (Wall.MultiTrail dirs) ->
          if
            Box.within levelBox newLocation
          then
            Update.Moved
              { level =
                { level
                | playerLocation =
                  newLocation
                , history =
                  Event.Move dir :: level.history
                , wallMap =
                  Trail.add level.playerLocation dir level.wallMap
                }
              , playerAnimation =
                playerAnimation
              }
          else
            Update.MoveFailed
        _ ->
          Update.MoveFailed
