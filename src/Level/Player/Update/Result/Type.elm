module Level.Player.Update.Result.Type
  exposing
    ( Result (..)
    )


import Level.Message.Type
  as Level
import Level.Model.Type
  as Level

type Result
  = Moved Level.Model
  | Pushed Level.Model
  | Pulled Level.Model
  | MoveFailed
