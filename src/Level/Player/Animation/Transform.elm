module Level.Player.Animation.Transform
  exposing
    ( fromAnimation
    )


import Animated.Completion
  as Animated
import Animated.Type
  exposing
    ( Animated
    )
import Level.Player.Animation.Type
  as Player
import Level.Player.Animation.Type
  as PlayerAnimation
import Util.Direction.ToOffset
  as Direction
import Util.Offset.Scale
  as Offset
import Util.Offset.ToFloat
  as Offset
import Util.Offset.Type
  exposing
    ( Offset
    )


fromAnimation : Maybe (Animated Player.Animation) -> String
fromAnimation animation =
  case
    animation
  of
    Nothing ->
      ""

    Just anim ->
      let
        animationOffset : Offset Float
        animationOffset =
          case
            anim.kind
          of
            PlayerAnimation.Movement dir ->
              Offset.scale
                ( Animated.completion anim
                |> (*) -1
                )
                ( Direction.toOffset dir
                |> Offset.toFloat
                )

            PlayerAnimation.Push dir ->
              Offset.scale
                ( Animated.completion anim
                |> (*) -1
                )
                ( Direction.toOffset dir
                |> Offset.toFloat
                )

            PlayerAnimation.Pull dir ->
              Offset.scale
                ( Animated.completion anim
                |> (*) -1
                )
                ( Direction.toOffset dir
                |> Offset.toFloat
                )

            PlayerAnimation.FailedMovement dir ->
              Offset.scale
                ( Animated.completion anim
                |> ( \ n ->
                     0.25 - (n - 0.5)^2
                   )
                )
                ( Direction.toOffset dir
                |> Offset.toFloat
                )
      in
        "translate("
          ++ String.fromFloat animationOffset.dx
          ++ " "
          ++ String.fromFloat animationOffset.dy
          ++ ")"

