module Level.Player.Animation.Type
  exposing
    ( Animation (..)
    )


import Util.Direction.Type
  exposing
    ( Direction
    )


type Animation
  = Movement Direction
  | Push Direction
  | Pull Direction
  | FailedMovement Direction
