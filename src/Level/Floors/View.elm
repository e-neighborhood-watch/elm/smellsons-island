module Level.Floors.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )


import Level.Floor.Type
  exposing
    ( Floor
    )
import Level.Floor.View
  as Floor
import Settings.Type
  exposing
    ( Settings
    )
import Util.Map.ToList
  as Map
import Util.Map.Type
  exposing
    ( Map
    )


view : Settings -> Map Int Floor -> Svg a
view settings floorMap =
  floorMap
  |> Map.toList
  |> List.map
    ( \ (pos, floor) ->
      Floor.view
        settings
        pos
        floor
    )
  |> Svg.g []
