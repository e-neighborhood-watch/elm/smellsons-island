module View
  exposing
    ( view
    )


import Browser
import Css
import Css.Global
  as Css
import Html
  as ElmHtml
import Html.Styled
  as Html
import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Data.Defs
  exposing
    ( defs
    )
import Document.Type
  exposing
    ( Document
    )
import Level.View
  as Level
import LevelSelect.View
  as LevelSelect
import Model.Type
  exposing
    ( Model
    , InnerModel (..)
    )
import Message.Type
  as Message
  exposing
    ( Message
    )
import Settings.Color.Theme
  as Theme
import Settings.View
  as Settings
import Settings.Wheel.View
  as SettingsWheel


mapDocument : (a -> Message) -> List Css.Snippet -> List (Svg Message)-> Document a -> Browser.Document Message
mapDocument f globalStyles extras { title, viewBox, body } =
  { title =
    title
  , body =
    [ Css.global
      globalStyles
      |> Html.toUnstyled
    , Svg.svg
      [ SvgAttr.viewBox
        ( String.fromFloat viewBox.min.x
        ++ " "
        ++ String.fromFloat viewBox.min.y
        ++ " "
        ++ String.fromFloat viewBox.max.x
        ++ " "
        ++ String.fromFloat viewBox.max.y
        )
      , SvgAttr.preserveAspectRatio "xMidYMid"
      , SvgAttr.css
        [ Css.position Css.absolute
        , Css.top Css.zero
        , Css.left Css.zero
        , Css.width (Css.pct 100)
        , Css.height (Css.pct 100)
        ]
      ]
      (defs :: List.map (Svg.map f) body ++ extras)
      |> Html.toUnstyled
    ]
  }


view : Model -> Browser.Document Message
view { model, levels, settings } =
  let
    globalStyles theme =
      [ Css.body
        [ Css.backgroundColor
          ( Color.Background
          |> Theme.toHex theme
          |> Css.hex
          )
        ]
      ]
  in
    case
      model
    of
      Level levelModel ->
        case
          Level.view settings levelModel
        of
          doc ->
            mapDocument
              ( always Message.Noop )
              ( globalStyles settings.theme )
              [ SettingsWheel.view doc.viewBox settings.theme
              ]
              doc
      LevelSelect ->
        case
          LevelSelect.view settings levels
        of
          doc ->
            mapDocument
              Message.LevelSelect
              ( globalStyles settings.theme )
              [ SettingsWheel.view doc.viewBox settings.theme
              ]
              doc
      Settings _ settingsModel ->
        settingsModel
          |> Settings.view
          |> mapDocument
             Message.Settings
             ( globalStyles settingsModel.settings.theme )
             [
             ]
