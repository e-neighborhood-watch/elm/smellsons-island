module Settings.Update.Result.Type
  exposing
    ( Result (..)
    )


import Settings.Model.Type
  exposing
    ( Model
    )


type Result
  = NoChange
  | ChangeModel Model
  | Cancel
  | SaveAndExit
