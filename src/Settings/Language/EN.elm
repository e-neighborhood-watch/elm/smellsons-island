module Settings.Language.EN
  exposing
    ( fromText
    )


import Data.Text.Type
  as Text
  exposing
    ( Text
    )


fromText : Text -> String
fromText text =
  case
    text
  of
    Text.HelloWorld ->
      "Hello, World!"
    Text.LevelSelectTitle ->
      "Level select"
    Text.SettingsTitle ->
      "Settings"
    Text.LevelsTitle ->
      "Levels"
