module Settings.Color.Theme.Light
  exposing
    ( toHex
    )


import Data.Color.Theme.Type
  as Color
  exposing
    ( ThemeColor
    )
import Data.Color.Type
  as Color


toHex : ThemeColor -> String
toHex color =
  case
    color
  of
    Color.Background ->
      "#CACACA"
    Color.Foreground ->
      "#343434"
    Color.Eyes ->
      "#CACACA"
    Color.Color Color.Red ->
      "#E44141"
    Color.Color Color.Orange ->
      "#CA7A32"
    Color.Color Color.Yellow ->
      "#D8DA21"
    Color.Color Color.Green ->
      "#49b549"
    Color.Color Color.Teal ->
      "#21DAAA"
    Color.Color Color.Blue ->
      "#5555EE"
    Color.Color Color.Purple ->
      "#8821DA"
