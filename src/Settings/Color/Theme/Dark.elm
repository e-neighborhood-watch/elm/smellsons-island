module Settings.Color.Theme.Dark
  exposing
    ( toHex
    )


import Data.Color.Theme.Type
  as Color
  exposing
    ( ThemeColor
    )
import Data.Color.Type
  as Color


toHex : ThemeColor -> String
toHex color =
  case
    color
  of
    Color.Background ->
      "#323232"
    Color.Foreground ->
      "#656565"
    Color.Eyes ->
      "#7b0000"
    Color.Color Color.Red ->
      "#7b0000"
    Color.Color Color.Orange ->
      "#904400"
    Color.Color Color.Yellow ->
      "#D8DA21"
    Color.Color Color.Green ->
      "#157900"
    Color.Color Color.Teal ->
      "#007466"
    Color.Color Color.Blue ->
      "#3232CA"
    Color.Color Color.Purple ->
      "#8821DA"
