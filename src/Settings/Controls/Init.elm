module Settings.Controls.Init
  exposing
    ( init
    )


import Settings.Controls.Type
  exposing
    ( Controls
    )


init : Controls
init =
  { levelSelectLeftPrevious =
    "a"
  , levelSelectLeftNext =
    "z"
  , levelSelectRightPrevious =
    "s"
  , levelSelectRightNext =
    "x"
  , levelSelectSelect =
    "Enter"
  , levelRestart =
    "r"
  , levelUndo =
    "u"
  , levelExit =
    "q"
  , playerMoveNorth =
    "ArrowUp"
  , playerMoveSouth =
    "ArrowDown"
  , playerMoveEast =
    "ArrowRight"
  , playerMoveWest =
    "ArrowLeft"
  }
