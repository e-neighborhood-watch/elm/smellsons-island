module Settings.Controls.Type
  exposing
    ( Controls
    )

type alias Controls =
  { levelSelectLeftPrevious :
    String
  , levelSelectLeftNext :
    String
  , levelSelectRightPrevious :
    String
  , levelSelectRightNext :
    String
  , levelSelectSelect :
    String
  , levelRestart :
    String
  , levelUndo :
    String
  , levelExit :
    String
  , playerMoveNorth :
    String
  , playerMoveSouth :
    String
  , playerMoveEast :
    String
  , playerMoveWest :
    String
  }
