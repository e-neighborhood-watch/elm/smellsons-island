module Settings.Font.Css
  exposing
    ( toCss
    )


import Css
import Svg.Styled
  exposing
    ( Attribute
    )
import Svg.Styled.Attributes
  as SvgAttr


import Settings.Font.Size.Css
  as FontSize
import Settings.Font.Size.Type
  exposing
    ( FontSize
    )
import Settings.Font.Type
  exposing
    ( Font
    )


toCss : Font -> FontSize -> Attribute msg
toCss font fontSize =
  SvgAttr.css
    [ Css.fontSize (FontSize.toPx font fontSize)
    , Css.fontFamilies
      [ "Lucida Console"
      , "Courier"
      , "sans-serif"
      ]
    ]
