module Animation.Movement.Type
  exposing
    ( Movement
    )


import Animated.Type
  exposing
    ( Animated
    )
import Util.Offset.Type
  exposing
    ( Offset
    )


type alias Movement =
  Animated (Offset Float)
