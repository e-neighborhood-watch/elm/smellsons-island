module Animation.Movement.Offset
  exposing
    ( offset
    )


import Animated.Completion
  as Animated
import Animation.Movement.Type
  exposing
    ( Movement
    )
import Util.Offset.Scale
  as Offset
import Util.Offset.Type
  exposing
    ( Offset
    )


offset : Movement -> Offset Float
offset movement =
  Offset.scale
    (Animated.completion movement)
    { dx =
      movement.kind.dx
    , dy =
      movement.kind.dy
    }
