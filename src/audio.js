(function(scope){
"use strict"

; var audioInit =
  function
    ()
    { let init =
      { tracks :
        {}
      }

    ; Object.defineProperty
      ( init
      , "context"
      , { value :
          new AudioContext()
        , configurable :
          false
        , writable :
          false
        }
      )

    ; Object.defineProperty
      ( init
      , "masterGain"
      , { value :
          init.context.createGain
            ()
        , configurable :
          false
        , writable :
          false
        }
      )

    ; init.masterGain.gain.value =
      1.0

    ; init.masterGain.connect
      ( init.context.destination
      )

    ; return init
    }

; var audioPortListener =
  ( audio
  , sendToPort
  ) =>
    function
      ( action
      )
      { switch
        (action.action)
        { case "initialize" :
            ; Object.keys
              ( audio.tracks
              ).forEach
                ( key =>
                  { const value =
                    audio.tracks[key]
                  ; value.gainNode.disconnect ()
                  ; value.mediaNode.disconnect ()
                  ; delete audio.tracks[key]
                  }
                )
            ; Object.keys
              ( action.data
              ).forEach
                ( key =>
                  { const data =
                    action.data[key]
                  ; const source =
                    new Audio(data.url)
                  ; source.oncanplaythrough =
                    (event) =>
                      sendToPort
                        ( { type :
                            "loadedFile"
                          , data :
                            key
                          }
                        )
                  ; source.loop =
                    data.music
                  ; const mediaNode =
                    audio.context.createMediaElementSource
                      ( source
                      )
                  ; const gainNode =
                    audio.context.createGain
                      ()
                  ; gainNode.gain.value =
                    data.music
                      ? 0.0
                      : 1.0
                  ; mediaNode.connect
                      ( gainNode
                      ).connect
                        ( audio.masterGain
                        )
                  ; audio.tracks[key] =
                    { source :
                      source
                    , mediaNode :
                      mediaNode
                    , gainNode :
                      gainNode
                    , music :
                      data.music
                    }
                  }
                )
            ; break

          case "start" :
            ; const audioTrack =
              audio.tracks[action.data.source]
            ; if
              ( !audioTrack
              )
              { sendToPort
                ( { type :
                    "startFailed"
                  , subtype :
                    "trackNotFound"
                  }
                )
              ; break;
              }
            ; if
              ( !audioTrack.music
              )
              { sendToPort
                ( { type :
                    "startFailed"
                  , subtype :
                    "trackNotMusic"
                  }
                )
              ; break;
              }
            ; let currentGain =
              audioTrack.gainNode.gain.value
            ; Object.values
              ( audio.tracks
              ).forEach
                ( trackToMute =>
                  { if
                      ( trackToMute.music
                      )
                      trackToMute.gainNode.gain.setValueAtTime
                      ( 0.0
                      , audio.context.currentTime
                      )
                  }
                )
            ; audioTrack.gainNode.gain.setValueAtTime
              ( currentGain
              , audio.context.currentTime
              )
            ; audioTrack.gainNode.gain.setTargetAtTime
              ( 1.0
              , audio.context.currentTime
              , action.data.timeConstant
              )
            ; const playPromises =
              []
            ; Object.values
              ( audio.tracks
              ).forEach
                ( trackToPlay =>
                  { if
                      ( trackToPlay.music
                      )
                      playPromises.push
                      ( trackToPlay.source.play ()
                      )
                  }
                )
            ; Promise.race
              ( playPromises
              ).catch
                ( () =>
                  sendToPort
                    ( { type :
                        "startFailed"
                      , subtype :
                        "tracksFailedToPlay"
                      }
                    )
                )
            ; break

          case "switch" :
            ; const newOnTrack =
              audio.tracks[action.data.source]
            ; if
              ( !newOnTrack
              )
              { sendToPort
                ( { type :
                    "switchFailed"
                  , subtype :
                    "trackNotFound"
                  }
                )
              ; break;
              }
            ; if
              ( !newOnTrack.music
              )
              { sendToPort
                ( { type :
                    "switchFailed"
                  , subtype :
                    "trackNotMusic"
                  }
                )
              ; break;
              }
            ; Object.values
              ( audio.tracks
              ).forEach
                ( trackToMute =>
                  { if
                    ( trackToMute.music
                    )
                    { trackToMute.gainNode.gain.setValueAtTime
                      ( trackToMute.gainNode.gain.value
                      , audio.context.currentTime
                      )
                    ; trackToMute.gainNode.gain.setTargetAtTime
                      ( 0.0
                      , audio.context.currentTime
                      , action.data.timeConstant
                      )
                    }
                  }
                )
            ; newOnTrack.gainNode.gain.setTargetAtTime
                ( 1.0
                , audio.context.currentTime
                , action.data.timeConstant
                )
            ; break

          case "play" :
            ; const playTrack =
              audio.tracks[action.data.source]
            ; if
              ( !playTrack
              )
              { sendToPort
                ( { type :
                    "playFailed"
                  , subtype :
                    "trackNotFound"
                  }
                )
              ; break;
              }
            ; if
              ( newOnTrack.music
              )
              { sendToPort
                ( { type :
                    "playFailed"
                  , subtype :
                    "trackIsMusic"
                  }
                )
              ; break;
              }
            ; playTrack.source.play().catch
              ( () =>
                sendToPort
                  ( { type :
                      "playFailed"
                    , subtype :
                      "trackFailedToPlay"
                    }
                  )
              )
            ; break

          case "mute" :
            ; audio.masterGain.gain.setValueAtTime
              ( audio.masterGain.gain.value
              , audio.context.currentTime
              )
            ; audio.masterGain.gain.setTargetAtTime
              ( 0.0
              , audio.context.currentTime
              , action.data
              )
            ; break

          case "unmute" :
            ; audio.masterGain.gain.setValueAtTime
              ( audio.masterGain.gain.value
              , audio.context.currentTime
              )
            ; audio.masterGain.gain.setTargetAtTime
              ( 1.0
              , audio.context.currentTime
              , action.data
              )
            ; break
        }
      }

; scope["GameAudio"] =
  { init :
    audioInit
  , portListener :
    audioPortListener
  }
}(this));
