module Model.Init
  exposing
    ( init
    )


import Data.Levels.Level1
  as Level1
import Data.Levels.Level2
  as Level2
import Data.Levels.Level3
  as Level3
import Data.Levels.Level4
  as Level4
import Data.Levels.Level5
  as Level5
import Data.Levels.Level6
  as Level6
import Data.Levels.Level7
  as Level7
import Data.Levels.Level8
  as Level8
import Data.Levels.Level9
  as Level9
import Data.Levels.Level10
  as Level10
import Data.Levels.Level11
  as Level11


import Level.Status.Type
  as Status
import LevelSelect.Init
  as LevelSelect
import Model.Type
  as Model
  exposing
    ( Model
    )
import Message.Type
  exposing
    ( Message
    )
import Settings.Init
  as Settings


init : a -> ( Model, Cmd Message )
init _ =
  ( { settings =
      Settings.init
    , levels =
      LevelSelect.init
        Level1.level
        Level1.level
        [
        ]
        [
        ]
        [ Level5.level
        , Level9.level
        , Level8.level
        , Level6.level
        , Level11.level
        ]
        [ Level6.level
        , Level2.level
        , Level4.level
        , Level3.level
        , Level7.level
        , Level10.level
        ]
    , model =
      Model.LevelSelect
    }
  , Cmd.none
  )

